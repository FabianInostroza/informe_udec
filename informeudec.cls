%%
%% Creado por Fabián Inostroza P.
%% fabianinostroza <arroba> udec <punto> cl
%%
%% ----------------------------------------------------------------------
%% 2015-09-09  F. Inostroza  Creacion, conversion de preambulo a clase
%% ----------------------------------------------------------------------
%%
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{informeudec}[2015/09/09 Informe UdeC]

\newif\if@colortitles
\@colortitlesfalse
\DeclareOption{colortitles}{\@colortitlestrue}

\DeclareOption*{%
    \PassOptionsToClass{\CurrentOption}{article}%
}

\ProcessOptions\relax

\LoadClass[12pt, letterpaper]{article}

\RequirePackage[spanish,es-tabla]{babel}
\spanishdecimal{.}
%\usepackage{amsmath}
\RequirePackage{mathtools}
\RequirePackage{amsfonts}
\RequirePackage{amssymb}
\RequirePackage{graphicx}
\RequirePackage{listings}
\RequirePackage{float}
%\DeclareGraphicsExtensions{.pdf,.png,.jpg}
\RequirePackage{booktabs}
\RequirePackage{array}
\RequirePackage[shortlabels]{enumitem}
%\RequirePackage{bm}

\RequirePackage[utf8]{inputenc}
\RequirePackage[T1]{fontenc}
\RequirePackage{microtype}

%\usepackage{enumerate}
%\setlist[description]{leftmargin=\parindent,labelindent=\parindent}
%\setlist[description]{labelwidth=1.5cm,labelindent=10pt,leftmargin=2.2cm}

% Variant B with superior spacing -- thanks to Peter Grill
%\SetLabelAlign{parright}{\parbox[t]{\labelwidth}{\raggedleft#1}}
%\setlist[description]{style=multiline,leftmargin=3cm,align=parright}

%\setlist[description]{style=multiline,topsep=10pt,leftmargin=5cm,font=\normalfont,%
%    align=parright}
%\newcommand{\comando}[num arg][valor defecto 1er arg(opcional)]{definicion}
%\lstloadlanguages{[ANSI]C}
\RequirePackage[svgnames]{xcolor}
\RequirePackage[hypcap=true]{caption}
\RequirePackage[hypcap=true]{subcaption}
\captionsetup{subrefformat=parens}

\RequirePackage{multicol}
%\IfFileExists{cmodif.tex}{\input{cmodif.tex}}{}
%\usepackage{hyperref}
\renewcommand{\maketitle}{
\begin{titlepage}
\noindent
  \includegraphics[width=2cm]{logoudec}
  \begin{tabular}{l}
    \textsc{Universidad de Concepción} \\
    \textsc{\@facultad} \\
    %
    \ifdefined\@depto
       \textsc{\@depto} \\
       \ifdefined\@carrera
           \textsc{\@carrera} \\
       \else
           \ \\
       \fi
    \else
       \ifdefined\@carrera
           \textsc{\@carrera} \\
           \\
       \else
          \\
          \\
       \fi
    \fi
%    \\
%    \\
%    \\
%    \\
%    \\
  \vspace*{12ex}
  \end{tabular}
 
  \vspace*{\fill}
  \begin{center}
  {\Huge\textbf{\Title}}
  \ifdefined\Subtitle
  
  \vspace{1em}
  {\Large \Subtitle}
  \fi
  \end{center}
  \vspace*{\fill} %
  \begin{flushright}
    \begin{tabular}{lrl}
\ifdefined\@authorTwo
\textbf{\textit{Alumnos}} &:&  \Author \\
      					& & \@authorTwo \\
\ifdefined\@authorThree
      					& & \@authorThree \\
\fi
\ifdefined\@authorFour
      					& & \@authorFour \\
\fi
\ifdefined\@authorFive
      					& & \@authorFive \\
\fi
\else
\textbf{\textit{Alumno}} &:&  \Author  \\
\fi
      \textit{\textbf{Profesor}} &:& \@profesor \\
\ifdefined\@ayudante
\ifdefined\@ayudanteDos
\textit{\textbf{Ayudantes}} &:& \@ayudante \\
                            & & \@ayudanteDos \\
\else
\textit{\textbf{Ayudante}} &:& \@ayudante \\
\fi
\fi
%      \textbf{\textit{Sección}} &:& 2 \\
%\textbf{\textit{Fecha de entrega}} &:& \fecha
\textbf{\textit{Fecha de entrega}} &:& \Date
%      \textbf{\textit{Fecha del laboratorio}} &:& \fechalab \\% \today \\
    \end{tabular}
\end{flushright}
\thispagestyle{empty}
\end{titlepage}
}
%\IfFileExists{../../pagtitulo.tex}{\input{../../pagtitulo.tex}}{\maketitle}
%\IfFileExists{pagtitulo.tex}{\input{pagtitulo.tex}}{\maketitle}


%% para citar y mostrar el texto completo.
%% El paquete IEEEtrantools y una entrada
%% adicional en la base de datos de la
%% bibliografia soluciona los problemas que
%% se producen cuando las ref tienen url
%% del README de hyperref
%  \makeatletter
%  \let\saved@bibitem\@bibitem
%  \makeatother
%
%\usepackage[retainorgcmds]{IEEEtrantools}
%\usepackage{bibentry} % \bibentry
%%\nobibliography*
%
%% redefinicion del comando bibentry para que los enlaces 
%% de las citas apunten a la bibliografia y no a donde se
%% usa el bibentry
%\makeatletter
%\newcommand\mybibentry[1]{{\frenchspacing
%     \@nameuse{BR@r@#1\@extra@b@citeb}}~\cite{#1}}
%%% original de bibentry.sty
%%\newcommand\bibentry[1]{\nocite{#1}{\frenchspacing
%%     \hyper@natanchorstart{#1\@extra@b@citeb}%
%%     \@nameuse{BR@r@#1\@extra@b@citeb}\hyper@natanchorend}}
%\makeatother


%\usepackage{fancyhdr}
%\fancypagestyle{normal}{%
%\fancyhf{}
%\renewcommand{\headrulewidth}{1.2pt} % customise the layout...
%\renewcommand{\footrulewidth}{0pt} % customise the layout...
%%\headheight=36pt 
%\fancyhead[L]{Laboratorio de Circuitos}
%\fancyfoot[C]{\thepage}}
%\pagestyle{fancy}

%\usepackage{tikz}
%\usepackage{pgfplots}
%\usepackage{pgfplotstable}
%\usepackage[siunitx,american,cuteinductor]{circuitikz}
%\usepackage{siunitx}
%\sisetup{load=derived} % loading \si
\RequirePackage{colortbl}
%\usetikzlibrary{plotmarks}
%\usetikzlibrary{scopes}
%\usepackage{steinmetz} %Notacion fasorial \phase
%\newcommand{\fasor}[2]{\ensuremath{\sqrt{(#1)^2+(#2)^2}\phase{\tan^{-1}\left(\frac{#2}{#1}\right)^{\circ}}}}

\newcommand{\fasorb}[2]{\ensuremath{#1 \phase{#2 ^{\circ}}}}
\newcommand{\txt}[1]{\text{#1}}

\RequirePackage[ruled,noend]{algorithm2e}
\SetKwIF{If}{ElseIf}{Else}{si}{}{sino si}{sino}{fin si}
%\SetKw{Return}{devolver}
\SetKwFor{For}{para}{hacer}{fin para}
%\SetKwFor{ForEach}{para cada}{hacer}{fin para}
%\SetKwFor{ForAll}{repetir}{veces}{fin repetir}
\SetKwFor{While}{mientras}{hacer}{fin mientras}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%  NUEVOS COMANDOS 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newcommand{\vect}[1]{\mathbf{#1}}
\newcommand{\vects}[1]{\boldsymbol{#1}}
%\newcommand{\vect}[1]{\bm{#1}}
%\newcommand{\vects}[1]{\bm{#1}}
\newcommand{\inc}[1]{\IfFileExists{#1.tex}{\include{#1}}{}}
\newcommand{\inp}[1]{\IfFileExists{#1.tex}{\input{#1}}{}}
\newcommand{\diff}[2]{\ensuremath{\frac{\text{d}#1}{\text{d}#2}}}
\newcommand{\pdiff}[2]{\ensuremath{\frac{\partial#1}{\partial#2}}}
\newcommand{\Tabcdq}{\ensuremath{%
\sqrt{\frac{2}{3}}
\begin{bmatrix}
\sin(\omega_1 t) &	\sin(\omega_1 t - 2\pi/3) & \sin(\omega_1 t - 4\pi/3) \\
\cos(\omega_1 t) &	\cos(\omega_1 t - 2\pi/3) & \cos(\omega_1 t - 4\pi/3) \\
\frac{1}{\sqrt{2}} & \frac{1}{\sqrt{2}} & \frac{1}{\sqrt{2}}
\end{bmatrix}
}}

\newcommand{\Tdqabc}{\ensuremath{%
\sqrt{\frac{2}{3}}
\begin{bmatrix}
\sin(\omega_1 t) &	\cos(\omega_1 t) & \frac{1}{\sqrt{2}} \\
\sin(\omega_1 t - 2\pi/3)  &	\cos(\omega_1 t - 2\pi/3) &\frac{1}{\sqrt{2}}  \\
 \sin(\omega_1 t - 4\pi/3)  & \cos(\omega_1 t - 4\pi/3) & \frac{1}{\sqrt{2}}
\end{bmatrix}
}}

\newcommand{\TTabcdq}{\ensuremath{\vect{T}_{abc}^{dq0}}}
\newcommand{\TTdqabc}{\ensuremath{\vect{T}_{dq0}^{abc}}}

\newcommand{\abs}[1]{\ensuremath{\left\lvert #1 \right\rvert}}
\newcommand{\norm}[1]{\ensuremath{\left\lVert #1 \right\rVert}}
\newcommand{\pf}{\text{p.f.}}
\newcommand{\vsIdq}{\ensuremath{\vect{v}_{s1}^{dq}}}
\newcommand{\vsIIdq}{\ensuremath{\vect{v}_{s2}^{dq}}}
\newcommand{\iIdq}{\ensuremath{\vect{i}_{i1}^{dq}}}
\newcommand{\iIIdq}{\ensuremath{\vect{i}_{i2}^{dq}}}
\newcommand{\vsIabc}{\ensuremath{\vect{v}_{s1}^{abc}}}
\newcommand{\vsIIabc}{\ensuremath{\vect{v}_{s2}^{abc}}}
\newcommand{\iIabc}{\ensuremath{\vect{i}_{i1}^{abc}}}
\newcommand{\iIIabc}{\ensuremath{\vect{i}_{i2}^{abc}}}
\newcommand{\vsIdqss}{\ensuremath{\vect{v}_{s1ss}^{dq}}}
\newcommand{\vsIIdqss}{\ensuremath{\vect{v}_{s2ss}^{dq}}}
\newcommand{\iIdqss}{\ensuremath{\vect{i}_{i1ss}^{dq}}}
\newcommand{\iIIdqss}{\ensuremath{\vect{i}_{i2ss}^{dq}}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\usepackage{color}
\definecolor{gray97}{gray}{.97}
\definecolor{gray75}{gray}{.75}
\definecolor{gray45}{gray}{.45}
\definecolor{tableau10_1}{RGB}{31,119,180}
\definecolor{tableau10_2}{RGB}{255,127,14}
\definecolor{tableau10_3}{RGB}{44,160,44}
\definecolor{tableau10_4}{RGB}{214,39,40}
\definecolor{tableau10_5}{RGB}{148,103,189}
\definecolor{tableau10_6}{RGB}{140,86,75}
\definecolor{tableau10_7}{RGB}{227,119,194}
\definecolor{tableau10_8}{RGB}{127,127,127}
\definecolor{tableau10_9}{RGB}{188,189,34}
\definecolor{tableau10_10}{RGB}{23,190,207}

\RequirePackage{sectsty}
%\chapterfont{\color{tableau10_1}}
%\sectionfont{\color{tableau10_1}}
%\subsectionfont{\color{tableau10_1}}
%\subsubsectionfont{\color{tableau10_1}}
\if@colortitles
\allsectionsfont{\raggedright\color{tableau10_1}}
\else
\allsectionsfont{\raggedright}
\fi

\RequirePackage{listings}
%\usepackage[numbered,framed]{matlab-prettifier}
\lstset{ frame=ltbr,
     framerule=1pt,
     aboveskip=0.5cm,
     framextopmargin=3pt,
     framexbottommargin=3pt,
     framexleftmargin=0.2cm,
     framesep=0pt,
     rulesep=.4pt,
%     backgroundcolor=\color{gray97},
     rulesepcolor=\color{black},
     %
     stringstyle=\ttfamily,
     showstringspaces = false,
     basicstyle=\small\ttfamily,
     commentstyle=\color{gray45},
     keywordstyle=\bfseries,
     %
     %numbers=left,
     numbersep=15pt,
     numberstyle=\tiny,
     numberfirstline = false,
     breaklines=true,
     tabsize=4,
     extendedchars=true,
}

\lstset{literate=
  {á}{{\'a}}1 {é}{{\'e}}1 {í}{{\'i}}1 {ó}{{\'o}}1 {ú}{{\'u}}1
  {Á}{{\'A}}1 {É}{{\'E}}1 {Í}{{\'I}}1 {Ó}{{\'O}}1 {Ú}{{\'U}}1
  {à}{{\`a}}1 {è}{{\`e}}1 {ì}{{\`i}}1 {ò}{{\`o}}1 {ù}{{\`u}}1
  {À}{{\`A}}1 {È}{{\'E}}1 {Ì}{{\`I}}1 {Ò}{{\`O}}1 {Ù}{{\`U}}1
  {ä}{{\"a}}1 {ë}{{\"e}}1 {ï}{{\"i}}1 {ö}{{\"o}}1 {ü}{{\"u}}1
  {Ä}{{\"A}}1 {Ë}{{\"E}}1 {Ï}{{\"I}}1 {Ö}{{\"O}}1 {Ü}{{\"U}}1
  {â}{{\^a}}1 {ê}{{\^e}}1 {î}{{\^i}}1 {ô}{{\^o}}1 {û}{{\^u}}1
  {Â}{{\^A}}1 {Ê}{{\^E}}1 {Î}{{\^I}}1 {Ô}{{\^O}}1 {Û}{{\^U}}1
  {œ}{{\oe}}1 {Œ}{{\OE}}1 {æ}{{\ae}}1 {Æ}{{\AE}}1 {ß}{{\ss}}1
  {ű}{{\H{u}}}1 {Ű}{{\H{U}}}1 {ő}{{\H{o}}}1 {Ő}{{\H{O}}}1
  {ç}{{\c c}}1 {Ç}{{\c C}}1 {ø}{{\o}}1 {å}{{\r a}}1 {Å}{{\r A}}1
  {€}{{\EUR}}1 {£}{{\pounds}}1 {ñ}{{\~n}}1
}
 
% minimizar fragmentado de listados
\lstnewenvironment{listing}[1][]
   {\lstset{#1}\pagebreak[0]}{\pagebreak[0]}
 
\lstdefinestyle{consola}
   {basicstyle=\scriptsize\bf\ttfamily,
    backgroundcolor=\color{gray75},
   }
   
\lstdefinestyle{mlab}{%
  %style              = Matlab-editor,
  %basicstyle         = \mlttfamily,
  %escapechar         = ",
  %mlshowsectionrules = true,
  language=MATLAB,
  keywordstyle=\color{tableau10_1},
  stringstyle=\color{tableau10_5},
  commentstyle=\color{tableau10_3},
  columns=fullflexible, % permite copiar codigo desde el pdf
% basicstyle=\scriptsize\ttfamily,
  basicstyle=\footnotesize\ttfamily,
  numbers=left,
  numbersep=15pt,
  morecomment=[l][\color{magenta}]{\#}
}


\lstdefinestyle{customc}{
  belowcaptionskip=1\baselineskip,
  breaklines=true,
  frame=L,
  xleftmargin=\parindent,
  language=C,
  showstringspaces=false,
  basicstyle=\footnotesize\ttfamily,
  keywordstyle=\bfseries\color{green!40!black},
  commentstyle=\itshape\color{purple!40!black},
  identifierstyle=\color{blue},
  stringstyle=\color{orange},
}

\lstdefinestyle{cmod}{
    language=C,
    keywordstyle=\color{blue},
    stringstyle=\color{red},
    commentstyle=\color{green},
%    basicstyle=\scriptsize\ttfamily,
    basicstyle=\footnotesize\ttfamily,
    morecomment=[l][\color{magenta}]{\#}
}

\renewcommand{\lstlistingname}{Listado}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\usepackage{lipsum} % generar texto de prueba
%\usepackage{blindtext} % generar texto de prueba

\DeclareGraphicsExtensions{.pdf,.jpeg,.png,.jpg,.ps,.eps}
\RequirePackage{multirow}
\RequirePackage{verbatim}

%\renewcommand\thesection{\Alph{section}}
%\renewcommand\thesubsection{\thesection.\alph{subsection}}

%\allowdisplaybreaks % global
% nottoc: no incluir ToC en el indice
\RequirePackage[nottoc]{tocbibind} 
\RequirePackage{tocloft}
% agregar puntos en el indice despues de las secciones
\renewcommand{\cftsecleader}{\cftdotfill{\cftdotsep}}
%\newcommand{\vect}[1]{\ensuremath{\mathbb{#1}}}
%\usepackage{appendix}

% numerar ecuaciones como seccion.numero
%\numberwithin{equation}{section}
\renewcommand{\figurename}{Figura}
\renewcommand{\tablename}{Tabla}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\newcommand{\profesor}[1]{\renewcommand\@profesor{#1}}
\newcommand\@profesor{\@latex@error{No \noexpand\profesor given}\@ehc}

\newcommand{\authorTwo}[1]{\newcommand\@authorTwo{#1}}

\newcommand{\authorThree}[1]{\newcommand\@authorThree{#1}}

\newcommand{\authorFour}[1]{\newcommand\@authorFour{#1}}

\newcommand{\authorFive}[1]{\newcommand\@authorFive{#1}}

\newcommand{\ayudante}[1]{\newcommand\@ayudante{#1}}
\newcommand{\ayudanteDos}[1]{\newcommand\@ayudanteDos{#1}}

\newcommand{\departamento}[1]{\newcommand\@depto{#1}}
\newcommand{\facultad}[1]{\renewcommand\@facultad{#1}}
\newcommand{\@facultad}{Facultad de Ingeniería}
\newcommand{\carrera}[1]{\newcommand\@carrera{#1}}
%
%\newcommand{\subtitle}[1]{\renewcommand\@subtitle{#1}}
%\newcommand\@subtitle{}
%\makeatother

\newcommand{\subtitle}[1]{\newcommand{\Subtitle}{#1}}
%\newcommand{\Subtitle}{\empty}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\DeclareUnicodeCharacter{00B0}{\ensuremath{^{\circ}}}
\newcommand{\Deg}{\ensuremath{^{\circ}}}

\RequirePackage[unicode=true,
                colorlinks=true,
                pdftoolbar=false,
                pdffitwindow=false,
                pdfstartview={FitH},
                pdfnewwindow=true,
                allcolors =MidnightBlue,
                ]{hyperref}

\AtBeginDocument{
\let\Date\@date
\let\Title\@title
\let\Author\@author
\hypersetup{
	bookmarksnumbered=true,
    unicode=true,          % non-Latin characters in Acrobat’s bookmarks
%    plainpages=false,
%    pdfpagelabels=true,
%    pdftoolbar=false,        % show Acrobat’s toolbar?
%    pdfmenubar=true,        % show Acrobat’s menu?
%    pdffitwindow=false,     % window fit to page when opened
%    pdfstartview={FitH},    % fits the width of the page to the window
    pdftitle={\Title{} -- \Subtitle},    % title
    pdfauthor={\Author},     % author
%    pdfsubject={Subject},   % subject of the document
%    pdfcreator={Creator},   % creator of the document
%    pdfproducer={Producer}, % producer of the document
%    pdfkeywords={keyword1} {key2} {key3}, % list of keywords
%    pdfnewwindow=true,      % links in new window
%    colorlinks=true,       % false: boxed links; true: colored links
%    allcolors  = MidnightBlue,
%    linkcolor=MidnightBlue,          % color of internal links (change box color with linkbordercolor)
%    linkbordercolor=tableau10_10,
%    citecolor=MidnightBlue,        % color of links to bibliography
%    filecolor=magenta,      % color of file links
%    urlcolor=MidnightBlue           % color of external links
}
}


\setcounter{secnumdepth}{5}
\setcounter{tocdepth}{5} % agrega \paragraph{} y \subparagraph{} al TOC

\endinput
